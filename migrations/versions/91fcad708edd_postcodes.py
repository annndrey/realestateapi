"""postcodes

Revision ID: 91fcad708edd
Revises: 91b8e9921215
Create Date: 2018-04-05 19:12:24.448186

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql

# revision identifiers, used by Alembic.
revision = '91fcad708edd'
down_revision = '91b8e9921215'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('postcode', sa.Column('suburb', sa.String(length=150), nullable=True))
    op.drop_column('postcode', 'fname')
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('postcode', sa.Column('fname', mysql.VARCHAR(length=150), nullable=True))
    op.drop_column('postcode', 'suburb')
    # ### end Alembic commands ###
