"""dbuser

Revision ID: 5a0be2d355da
Revises: 8a4f85d43746
Create Date: 2018-04-05 19:52:51.927885

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '5a0be2d355da'
down_revision = '8a4f85d43746'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('user',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('login', sa.String(length=400), nullable=True),
    sa.Column('password', sa.String(length=400), nullable=True),
    sa.Column('note', sa.Text(), nullable=False),
    sa.PrimaryKeyConstraint('id')
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('user')
    # ### end Alembic commands ###
