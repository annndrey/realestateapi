#!/usr/bin/python
# -*- coding: utf-8 -*-


from flask import Flask, jsonify, request, make_response, g
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_restful import Resource, Api, reqparse, abort
from flask_httpauth import HTTPBasicAuth
from flask_marshmallow import Marshmallow
from flask_restful_swagger import swagger

from models import db, User, ActivityType, Postcode, Contact, Property, Activity, Appraisal, Note

# TODO доделать документацию
app = Flask(__name__)
ma = Marshmallow(app)
auth = HTTPBasicAuth()

app.config.from_object('config.Config')
# app.config.from_object('config.ProductionConfig')
db.init_app(app)
migrate = Migrate(app, db)
api=Api(app)
api = swagger.docs(api, apiVersion='1', api_spec_url="/api/v1/spec")

# TODO - arg parser
# swagger для описания api

@auth.verify_password
def verify_password(username_or_token, password):
    user = User.verify_auth_token(username_or_token)
    if not user:
        # try to authenticate with username/password
        user = User.query.filter_by(login = username_or_token).first()
        if not user or not user.verify_password(password):
            return False
    g.user = user
    return True

# Schema, Get, Post, Put, Delete
# +++User
# +++Contact
# +++Property
# +Activity
# Appraisal
# Note

@app.route('/api/token')
@auth.login_required
def get_auth_token():
    token = g.user.generate_auth_token()
    return jsonify({ 'token': token.decode('ascii') })


class Hello(Resource):
    # @auth.login_required
    """Main entry point"""
    @swagger.operation(notes="Returns current API version")
    def get(self):
        return {'api ver': app.config['API_VERSION']}


# schemas
# TODO marshmallow

class NoteSchema(ma.ModelSchema):
    class Meta:
        model = Note

class ActivitySchema(ma.ModelSchema):
    class Meta:
        model = Activity
        include_fk = True

    notes = ma.Nested(NoteSchema, many=True)

class UserSchema(ma.ModelSchema):
    class Meta:
        model = User


class ContactSchema(ma.ModelSchema):
    class Meta:
        model = Contact
        include_fk = True

    postcode_name = ma.Function(lambda obj: obj.postcode.postcode)
    suburb = ma.Function(lambda obj: obj.postcode.suburb)
    activities = ma.Nested(ActivitySchema, many=True)


class AppraisalSchema(ma.ModelSchema):
    class Meta:
        model = Appraisal
        include_fk = True

    contacts = ma.Nested(ContactSchema, many=True)


class PropertySchema(ma.ModelSchema):
    class Meta:
        model = Property
        include_fk = True

    postcode_name = ma.Function(lambda obj: obj.postcode.postcode)
    suburb = ma.Function(lambda obj: obj.postcode.suburb)
    contacts = ma.Nested(ContactSchema, many=True)



# models

class Users(Resource):
    user_schema = UserSchema()
    @auth.login_required
    @swagger.operation(notes="List of users or user details if user id is provided")
    def get(self, userid=None):
        if userid is None:
            users = User.query.all()
            return jsonify(results = [self.user_schema.dump(u) for u in users])

        else:
            user = User.query.filter_by(id = userid).first()
            if user is not None:
                return self.user_schema.dump(user)
            else:
                abort(404)

    @auth.login_required
    #@swagger.operation(notes="Creation of a new user")
    def post(self):
        request.get_json()
        if request.json is None:
            abort(400, message="No data provided")
        username = request.json.get('username')
        password = request.json.get('password')
  
        if username is None or password is None:
            abort(400) # missing arguments
        if User.query.filter_by(login = username).first() is not None:
            abort(400) # existing user

        user = User(login = username)
        user.hash_password(password)
        user.note = "test"
        db.session.add(user)
        db.session.commit()

        return make_response(self.user_schema.dump(user), 201, {'Location': api.url_for(Users, userid = user.id)})

class PropertiesContacts(Resource):
    contact_schema = ContactSchema()
    @auth.login_required
    def get(self, propertyid=None, contactid=None):
        if propertyid is None:
            abort(400, message="No property id provided")
        
        if contactid is None:
            prop = Property.query.filter_by(id = propertyid).first()
            if prop is not None:
                return jsonify(results = [self.contact_schema.dump(c) for c in prop.contacts])
        else:
            contact = Contact.query.filter_by(id = contactid).first()
            if contact is not None:
                return jsonify(results=[self.contact_schema.dump(contact),])
            else:
                abort(404)

    @auth.login_required
    def post(self, propertyid=None):
        pass

    @auth.login_required
    def put(self, propertyid=None, contactid=None):
        pass

    @auth.login_required
    def delete(self, propertyid=None, contactid=None):
        pass


class PropertiesAppraisals(Resource):
    appraisal_schema = AppraisalSchema()
    @auth.login_required
    def get(self, propertyid=None, appraisalid=None):
        if propertyid is None:
            abort(400, message="No property id provided")
        
        if appraisalid is None:
            prop = Property.query.filter_by(id = propertyid).first()
            if prop is not None:
                return jsonify(results = [self.contact_schema.dump(a) for a in prop.appraisals])
        else:
            appraisal = Appraisal.query.filter_by(id = appraisalid).first()
            if appraisal is not None:
                return jsonify(results=[self.appraisal_schema.dump(appraisal),])
            else:
                abort(404)

    @auth.login_required
    def post(self, propertyid=None):
        pass

    @auth.login_required
    def put(self, propertyid=None, appraisal=None):
        pass

    @auth.login_required
    def delete(self, propertyid=None, appraisalid=None):
        pass




class ContactsActivities(Resource):
    activity_schema = ActivitySchema()
    @auth.login_required
    def get(self, contactid=None, activityid=None):
        if contactid is None:
            abort(400, message="No contact id provided")
        
        if activityid is None:
            contact = Contact.query.filter_by(id = contactid).first()
            if contact is not None:
                return jsonify(results = [self.activity_schema.dump(a) for a in contact.activities])
        else:
            activity = Activity.query.filter_by(id = activityid).first()
            if activity is not None:
                return jsonify(results=[self.activity_schema.dump(activity),])
            else:
                abort(404)

    @auth.login_required
    def post(self, contactid=None):
        pass

    @auth.login_required
    def put(self, contactid=None, activityid=None):
        pass

    @auth.login_required
    def delete(self, contactid=None, activityid=None):
        pass


class Contacts(Resource):
    contact_schema = ContactSchema()
    @auth.login_required
    #@swagger.operation(notes="List of contacts or contact details if contact id is provided")
    def get(self, contactid=None):
        if contactid is None:
            contacts = Contact.query.all()
            # TODO посмотреть какие поля выдавать
            return jsonify(results = [self.contact_schema.dump(c) for c in contacts])
        else:
            contact = Contact.query.filter_by(id = contactid).first()
            if contact is not None:
                return jsonify(results=[self.contact_schema.dump(contact),])
            else:
                abort(404)

    @auth.login_required
    #@swagger.operation(notes="New contact creation")
    def post(self):
        resp = {}
        request.get_json()
        fname = request.json.get('fname')
        lname = request.json.get('lname')
        landline = request.json.get('landline')
        mobile = request.json.get('mobile')
        email = request.json.get('email')
        address = request.json.get('address')
        unitnum = request.json.get('unitnum')
        streetnum = request.json.get('streetnum')
        # suburb = request.json.get('suburb')
        postcode = request.json.get('postcode')
        property_linked_status = request.json.get('property_linked_status')
        notes = request.json.get('notes')
        property_id = request.json.get('property_id')
        appraisal_id = request.json.get('appraisal_id')
        # 
        # здесь создаем new Activity
        # landline or mobile is not null
        if landline is None or mobile is None:
            abort(400, message="Landline or mobile are mandatory")
        if fname is None or lname is None:
            abort(400, message="Firstname or lastname are mandatory")
            
        pcode = Postcode.query.filter_by(postcode=postcode).first()
        if postcode is None:
            abort(400, message="Wrong postcode {0}".format(postcode))
        

        # TODO Unique keys for landline|mobile|fname|lname
        # TODO regex validators for phones

        newcontact = Contact()
        newcontact.fname = fname
        newcontact.lname = lname
        newcontact.landline = landline
        newcontact.mobile = mobile
        newcontact.email = email
        newcontact.address = address
        newcontact.unitnum = unitnum
        newcontact.streetnum = streetnum
        newcontact.suburb = pcode.suburb
        newcontact.postcode_id = pcode.id
        if property_id is not None:
            prop = Property.query.filter_by(id=property_id).first()
            if prop is not None:
                newcontact.property_linked_status = True
                newcontact.property_id = prop.id
        db.session.add(newcontact)
        db.session.commit()
        newactivity = Activity(contact_id=newcontact.id)
        db.session.add(newactivity)
        db.session.commit()

        return make_response(self.contact_schema.dump(newcontact), 201, {'Location': api.url_for(Contacts, contactid = newcontact.id)})


class Properties(Resource):
    property_schema = PropertySchema()
    @auth.login_required
    #@swagger.operation(notes="List of contacts or contact details if contact id is provided")
    def get(self, propertyid=None):
        if propertyid is None:
            properties = Property.query.all()
            # TODO посмотреть какие поля выдавать
            return jsonify(results = [self.property_schema.dump(p) for p in properties])
        else:
            prop = Property.query.filter_by(id = propertyid).first()
            if prop is not None:
                return self.property_schema.dump(prop)
            else:
                abort(404)

    @auth.login_required
    #@swagger.operation(notes="New contact creation")
    def post(self):
        resp = {}
        request.get_json()
        address = request.json.get('address')
        unitnum = request.json.get('unitnum')
        streetnum = request.json.get('streetnum')
        postcode = request.json.get('postcode')
        # landline or mobile is not null
        for par in [address, unitnum, streetnum, postcode, statusflag]:
            if par is None:
                abort(400, message="%s mandatory" % par.__name__)
            
        pcode = Postcode.query.filter_by(postcode=postcode).first()
        if postcode is None:
            abort(400, message="Wrong postcode {0}".format(postcode))

        newproperty = Property(address = address, 
                              unitnum = unitnum,
                              streetnum = streetnum,
                              suburb = pcode.suburb,
                              postcode_id = pcode.id,
                              statusflag = statusflag
                              )
        db.session.add(newproperty)
        db.session.commit()

        return make_response(self.property_schema.dump(propertycontact), 201, {'Location': api.url_for(Properties, propertyid = newproperty.id)})


# urls
api.add_resource(Hello, '/')
api.add_resource(Users, '/api/users', '/api/users/<int:userid>')
api.add_resource(ContactsActivities,
                 '/api/contacts/<int:contactid>/activities',
                 '/api/contacts/<int:contactid>/activities/<int:activityid>'
                 )
api.add_resource(PropertiesContacts,
                 '/api/properties/<int:propertyid>/contacts',
                 '/api/properties/<int:propertyid>/contacts/<int:contactid>'
                 )

api.add_resource(PropertiesAppraisals,
                 '/api/properties/<int:propertyid>/appraisals',
                 '/api/properties/<int:propertyid>/appraisals/<int:appraisalid>'
                 )

api.add_resource(Contacts, 
                 '/api/contacts', 
                 '/api/contacts/<int:contactid>'
                 )
api.add_resource(Properties,
                 '/api/properties', 
                 '/api/properties/<int:propertyid>',
                 )

# appraisal notes
# activity notes
# contact notes

# api.add_resource(Note, '/api/notes', '/api/notes/<int:noteidid>')

if __name__ == '__main__':
    app.run()
