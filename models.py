#!/usr/bin/python
# -*- coding: utf-8 -*-

import datetime
import enum

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import CheckConstraint
from passlib.apps import custom_app_context as pwd_context
from itsdangerous import (TimedJSONWebSignatureSerializer as Serializer, BadSignature, SignatureExpired)
from flask import current_app

db = SQLAlchemy()

class ActivityType(enum.Enum):
    nophone = "No phone number(4)"
    rangout = "Phone rang out(5)"
    notconnected = "Phone not connected(4)"
    potential = "Potential(7)"
    post = "Post(8)"
    connectiton = "Phone call (connection)(1)"
    ondatabase = "On DataBase(1)"
    succknocked = "Door knocked (success)"
    unsuccknocked = "Door knocked (unseccessful)"
    leftvoicemessage = "Left voice message"
    cardsent = "Card sent"


class StatusFlag(enum.Enum):
    newdata = "Recently added property"
    status1 = "Status 1"
    status2 = "Status 2"


class Postcode(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    postcode = db.Column(db.Integer())
    suburb = db.Column(db.String(400))


class Contact(db.Model):
    # checkconstraint doesnt work in mysql
    id = db.Column(db.Integer, primary_key=True)
    fname = db.Column(db.String(150))
    lname = db.Column(db.String(150))
    # if one is provided, other is optional 
    # landline or mobile is not null
    # "02 2235 3434"
    landline = db.Column(db.String(15))
    # "0415 123 123"
    mobile = db.Column(db.String(15))
    email = db.Column(db.String(150))
    address = db.Column(db.Text())
    unitnum = db.Column(db.String(15))
    streetnum = db.Column(db.String(15))
    # suburb is from postcodes
    # suburb = db.Column(db.String(150))
    # ENUM
    postcode = db.relationship('Postcode', backref='contacts', lazy=True)
    postcode_id = db.Column(db.Integer, db.ForeignKey('postcode.id'))
    property_linked_status = db.Column(db.Boolean, default=False)
    # FK
    notes = db.relationship('Note', backref='contact', lazy=True)
    property_id = db.Column(db.Integer, db.ForeignKey('property.id'))
    appraisal_id = db.Column(db.Integer, db.ForeignKey('appraisal.id'))
    activities = db.relationship('Activity', backref='contact', lazy=True)


class Property(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    address = db.Column(db.Text(), nullable=False)
    unitnum = db.Column(db.String(15), nullable=False)
    streetnum = db.Column(db.String(15), nullable=False)
    suburb = db.Column(db.String(150), nullable=False)
    postcode = db.relationship('Postcode', backref='properties', lazy=True)
    postcode_id = db.Column(db.Integer, db.ForeignKey('postcode.id'))
    # ENUM what as default?
    statusflag = db.Column(db.Enum(StatusFlag), nullable=False, default=StatusFlag.newdata)
    # FK
    contacts = db.relationship('Contact', backref='property', lazy=True)


class Activity(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    # FK -> id из Contact_table (key)
    timestamp = db.Column(db.DateTime(), nullable=False, default=datetime.datetime.utcnow)
    # ENUM
    activity = db.Column(db.Enum(ActivityType), nullable=False, default=ActivityType.ondatabase)
    # FK -> Note_id (key)
    contact_id = db.Column(db.Integer, db.ForeignKey('contact.id'))
    note = db.relationship('Note', backref='activity', lazy=True)
    note_id = db.Column(db.Integer, db.ForeignKey('note.id'))


class Appraisal(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    timestamp = db.Column(db.DateTime(), nullable=False, default=datetime.datetime.utcnow)
    # FK -> Contact Id может быть больше чем один
    _property = db.relationship('Property', backref='appraisal', lazy=True)
    property_id = db.Column(db.Integer, db.ForeignKey('property.id'))
    contacts = db.relationship('Contact', backref='appraisal', lazy=True)
    note = db.relationship('Note', backref='appraisal', lazy=True)
    note_id = db.Column(db.Integer, db.ForeignKey('note.id'))


class Note(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    timestamp = db.Column(db.DateTime(), nullable=False, default=datetime.datetime.utcnow)
    text = db.Column(db.Text(), nullable=False)
    contact_id = db.Column(db.Integer, db.ForeignKey('contact.id'))


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    login = db.Column(db.String(400))
    password_hash = db.Column(db.String(400))
    note = db.Column(db.Text(), nullable=True)
    
    def hash_password(self, password):
        self.password_hash = pwd_context.encrypt(password)

    def verify_password(self, password):
        return pwd_context.verify(password, self.password_hash)

    def generate_auth_token(self, expiration = 600):
        s = Serializer(current_app.config['SECRET_KEY'], expires_in = expiration)
        return s.dumps({ 'id': self.id })

    @staticmethod
    def verify_auth_token(token):
        s = Serializer(current_app.config['SECRET_KEY'])
        try:
            data = s.loads(token)
        except SignatureExpired:
            return None 
        except BadSignature:
            return None 
        user = User.query.get(data['id'])

        return user
