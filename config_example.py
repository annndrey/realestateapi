class Config(object):
    DEBUG = True
    DEVELOPMENT = True
    SECRET_KEY='mysupersecretket'
    SQLALCHEMY_DATABASE_URI='mysql+pymysql://user:password@localhost/realestate'
    SQLALCHEMY_TRACK_MODIFICATIONS = False


class ProductionConfig(Config):
    DEVELOPMENT = False
    DEBUG = False
